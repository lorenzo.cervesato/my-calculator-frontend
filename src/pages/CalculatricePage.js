import React, {useState} from 'react';
import { Button, Card } from 'react-bootstrap';
import LogoCasio from '../components/logo';

import './App.css';
import BoutonEgal from '../components/BoutonEgal';

const CalculatricePage = () => {
  const [value,setValue] = useState('0');

const handleClick = (event) => {
  switch(value){
    // Cas: Initialisation, aucun bouton n'a été pressé.
    case '0':
      switch(event.target.value){
        // Interdire l'ajout au state des valeurs des boutons suivants:
        case 'AC':
        case 'DEL':
        case '+':
        case 'x':
        case '÷':
        case ',':
        case '=':
          // Alors rien de ne passe.
          break;
        // Les autres sont autorisés et remplacent le state initial.
        default:
          setValue(event.target.value);
          break;
      }
      break;
    // Cas: en cours d'utilisation
    default:
      switch(event.target.value){
        // Si l'on presse l'un des boutons suivants:
        case '+':
        case '-':
        case 'x':
        case '÷':
          switch(value.slice(-1)){
            // Et que le dernier caractère ajouté au state est l'un de ceux-ci:
            case '+':
            case '-':
            case 'x':
            case '÷':
              // On interdit l'ajout au state des valeurs des boutons pressés.
              // Rien ne se passe.
              break;
            default:
              setValue(value + event.target.value);
              break;
          }
          break;
        case ',':
          // Le cas de la virgule est un cas spécial
          if(value.slice(-1) !== ','){
            setValue(value + event.target.value);
          }
        break;
        case 'AC':
          setValue('0');
          break;
        case 'DEL':
          if(value.length > 1){
            const str = value.substring(0, value.length - 1);
            setValue(str);
          }else{
            setValue('0');
          }
          break;
        case '=':
            break;
        //Dans les autres cas, on ajoute au state existant la valeur du bouton pressé.
        default:
          setValue(value + event.target.value);
          break;
      }
      break;
  }
}

var clav_elements = ['AC', 'DEL', '÷', '7', '8', '9', 'x', '4', '5', '6', '-', '1', '2', '3', '+', '0', ','];

  return (
    <>
    <Card variant="outlined" style={{backgroundColor:'black', width: '80%', margin: 'auto', padding: '20px'}}>
        <LogoCasio/>
        <hr></hr>
        <form className="calc_form">
            <input type="text" disabled={true} value={value} />
            <hr></hr>
            {clav_elements.map((val,i) => <Button variant="outline-light" onClick={(e) => handleClick(e)} className={"bouton"+(i+1)} key={"bouton"+(i+1)} value={val}>{val}</Button>)}
            <BoutonEgal value={value} setValue={setValue} />
        </form>
    </Card>
    </> 
);
}

export default CalculatricePage;