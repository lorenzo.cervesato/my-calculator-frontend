import React, { useState, useEffect } from "react";
import { Button } from 'react-bootstrap';
import axios from 'axios';

const BoutonEgal = (props) => {

    const [submit, setSubmit] = useState(0);

    useEffect(() => {
        // Remplacement des signes pour le calcul javascript.
        var strToSend = props.value.replaceAll('x', '*');
        strToSend = strToSend.replaceAll('÷', '/');
        strToSend = strToSend.replaceAll(',', '.');

        axios.post('http://localhost:3001/calculate', {
          toCalculate: strToSend,
        })
        .then(function (response) {
          var strToShow = response.data.response.replaceAll('.', ',');
          props.setValue(strToShow);
        })
        .catch(function (error) {
          console.log(error);
        });
  
        return setSubmit(0);
    },[submit]);

    const handleSubmit = () => {
        setSubmit(submit+1);
    }

    return (
        <Button variant="outline-light" onClick={handleSubmit} className={"boutonSubmit"} key={"boutonSubmit"} value="=">=</Button>
    );
};

export default BoutonEgal;